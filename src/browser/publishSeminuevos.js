import { resolve } from 'path';
import setupSeminuevos from './setupSeminuevos';
import carDetailsForm from './carDetailsForm';
import carPreviewForm from './carPreviewForm';
import packageForm from './packageForm';

export default async function publishSeminuevos({
  brand,
  model,
  subtype,
  year,
  state,
  city,
  kmTraveled,
  price,
  description,
  photos,
}) {
  const { browser, page } = await setupSeminuevos();

  await carDetailsForm(page, {
    brand,
    model,
    subtype,
    year,
    state,
    city,
    kmTraveled,
    price,
  });

  await carPreviewForm(page, { description, photos });

  await packageForm(page);

  await page.screenshot({ path: resolve('temp/screenshot.png'), fullPage: true });

  await browser.close();
}
