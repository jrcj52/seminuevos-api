import normalize from '../utils/normalize';

/**
 * @param {import("puppeteer").Page} page,
 * @param {string} selector
 * @param {string} value
 */
export default async function setSelectionField(page, selector, value) {
  console.log(`fill ${selector} with "${value}"`);
  // eslint-disable-next-line no-shadow
  await page.evaluate(({ selector }) => {
    // eslint-disable-next-line no-undef
    const container = $(`.latam-dropdown:has(${selector})`);
    container.attr('id', `${selector.slice(1)}-container`);
  }, { selector });

  const container = await page.$(`${selector}-container`);

  (await container.$('a.latam-dropdown-button:first-of-type i')).click();
  await page.waitFor(100);
  (await container.$('input')).focus();
  await page.waitFor(100);
  await page.keyboard.type(normalize(value));
  await page.waitFor(1000);

  (await container.$('ul li:first-child a')).click();
  await page.waitFor(1000);
}
