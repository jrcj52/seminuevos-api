import puppeteer from 'puppeteer';

export default async function setupSeminuevos() {
  console.log('setup browser');
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({ width: 1250, height: 1000 });

  await page.goto('https://www.seminuevos.com/login');
  await page.waitFor(1000);

  console.log('login fields');
  // account
  (await page.$('#email_login')).focus();
  await page.keyboard.type(process.env.SEMINUEVOS_ACCOUNT);

  // password
  (await page.$('#password_login')).focus();
  await page.keyboard.type(process.env.SEMINUEVOS_PASSWORD);

  // await page.screenshot({ path: 'filled.png' });

  (await page.$('#sigin-form button[type=submit]')).click();

  await page.waitForNavigation();
  console.log('logged in');
  // await page.screenshot({ path: 'end.png' });

  return { browser, page };
}
