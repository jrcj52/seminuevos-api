import setSelectionField from './setSelectionField';

/**
 * @param { import("puppeteer").Page } page
 */
export default async function carDetailsForm(page, {
  brand,
  model,
  subtype,
  year,
  state,
  city,
  kmTraveled,
  price,
}) {
  console.log('nagivate to wizard');
  await page.goto('https://www.seminuevos.com/wizard');
  await page.waitFor(5000);

  await setSelectionField(page, '#dropdown_brands', brand);
  await setSelectionField(page, '#dropdown_models', model);
  await setSelectionField(page, '#dropdown_subtypes', subtype);
  await setSelectionField(page, '#dropdown_years', year);
  await setSelectionField(page, '#dropdown_provinces', state);
  await setSelectionField(page, '#dropdown_cities', city);

  (await page.$('#input_recorrido')).focus();
  await page.keyboard.type(kmTraveled);
  await page.waitFor(300);

  (await page.$('#input_precio')).focus();
  await page.keyboard.type(price);
  await page.waitFor(300);

  (await page.$('.next-button')).click();
  await page.waitForNavigation();
  await page.waitFor(2000);
  // https://www.seminuevos.com/wizard
}
