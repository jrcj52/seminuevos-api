import { resolve } from 'path';
import { Router } from 'express';
import { publishSeminuevos } from '../browser';

const postCar = Router();

/*
Tipo: autos
Marca: Acura
Modelo: ILX
Subtipo: Sedán
Año: 2018
Estado: Nuevo León
Ciudad/Delegación: Monterrey
Recorrido: 20000 kms
Precio: [INPUT DE USUARIO]
Transacción: Negociable
Descripción: [INPUT DE USUARIO]
Imágenes: Sube 3 fotos, las que sean.
Paquete: Free
*/

postCar.post('/car', async (req, res, next) => {
  try {
    console.log(' - - - - Response begin');
    const { price, description } = req.body;

    await publishSeminuevos({
      brand: 'Acura',
      model: 'ILX',
      subtype: 'Sedán',
      year: '2018',
      state: 'Nuevo León',
      city: 'Monterrey',
      kmTraveled: '20000',
      phone: '9996663330',
      price,
      transaction: 'Negociable',
      description,
      photos: [
        resolve('images/1.jpg'),
        resolve('images/2.jpg'),
        resolve('images/3.jpg'),
      ],
    });

    res.sendFile(resolve('temp/screenshot.png'));
    console.log(' - - - - Response end');
  } catch (error) {
    next(error);
  }
});

export default postCar;
