/**
 * @param { import("puppeteer").Page } page
 */
export default async function carPreviewForm(page, { description, photos }) {
  (await page.$('textarea')).focus();
  await page.keyboard.type(description);

  const filesInput = await page.$('#Uploader');
  filesInput.uploadFile(...photos);

  await page.waitFor(5000);

  (await page.$('.next-button:last-child')).click();

  await page.waitForNavigation();
  await page.waitFor(2000);
}
