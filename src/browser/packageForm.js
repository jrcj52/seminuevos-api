/**
 * @param { import("puppeteer").Page } page
 */
export default async function packageForm(page) {
  (await page.$('#cancelButton')).click();
  await page.waitForNavigation();
  await page.waitFor(100);
}
