export default function normalize(input) {
  return input.replace(/[áéíóúÁÉÍÓÚ]/g, '');
}
